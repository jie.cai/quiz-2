package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class TimezonesControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_get_timezones() throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                get("/api/timezones")
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(jsonPath("$").isArray())
                .andReturn();

        List timezones = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(), List.class);

        assertTrue(timezones.contains("Africa/Abidjan"));
        assertTrue(timezones.contains("Africa/Accra"));
    }
}
