package com.twuc.webApp.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class StaffControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_create_a_rob_ok() throws Exception {
        mockMvc.perform(
                post("/api/staffs")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"firstName\": \"Rob\",\n" +
                                "  \"lastName\": \"Hall\"\n" +
                                "}")
        )
                .andExpect(status().isCreated())
                .andExpect(header().exists("Location"));
    }

    @Test
    void should_not_create_a_rob_when_first_name_blank() throws Exception {
        mockMvc.perform(
                post("/api/staffs")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"firstName\": \"\",\n" +
                                "  \"lastName\": \"Hall\"\n" +
                                "}")
        )
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_not_create_a_rob_when_over_first_name_length() throws Exception {
        mockMvc.perform(
                post("/api/staffs")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"firstName\": \"12345678901234567890123456789012345678901234567890123456789012345\",\n" +
                                "  \"lastName\": \"Hall\"\n" +
                                "}")
        )
                .andExpect(status().isBadRequest());
    }

    private String createStaff() throws Exception {
        return createStaff("Rob", "Hall");
    }
    private String createStaff(String firstName, String lastName) throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                post("/api/staffs")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"firstName\": \""+firstName+"\",\n" +
                                "  \"lastName\": \""+lastName+"\"\n" +
                                "}")
        )
                .andExpect(status().isCreated())
                .andReturn();
        return mvcResult.getResponse().getHeader("Location");
    }

    @Test
    void should_get_created_staff() throws Exception {
        mockMvc.perform(
            get(createStaff("hello", "world"))
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(any(Integer.class)))
                .andExpect(jsonPath("$.firstName").value("hello"))
                .andExpect(jsonPath("$.lastName").value("world"));
    }

    @Test
    void should_not_get_staff() throws Exception {
        mockMvc.perform(
                get(createStaff() + "233333")
        )
                .andExpect(status().isNotFound());
    }

    @Test
    void should_get_staff_list() throws Exception {
        mockMvc.perform(
                get("/api/staffs")
        )
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    void should_update_staff_zone_id() throws Exception {
        String staffUri = createStaff("set", "zoneId");
        mockMvc.perform(
                put(staffUri + "/timezone")
                        .content("{\n" +
                                "  \"zoneId\": \"Asia/Chongqing\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk());
    }
}
