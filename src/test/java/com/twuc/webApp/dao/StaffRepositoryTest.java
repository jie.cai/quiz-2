package com.twuc.webApp.dao;

import com.twuc.webApp.entity.Staff;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class StaffRepositoryTest {
    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    void should_save_and_find_staff() {
        Staff savedStaff = staffRepository.save(new Staff("first", "last"));
        assertNotNull(staffRepository.findById(savedStaff.getId()));
    }

    @Test
    void should_set_zone_id() {
        Staff staff = staffRepository.save(new Staff("first", "last"));

        String expectZoneId = "Africa/Abidjan";

        staff.setZoneId(expectZoneId);
        entityManager.flush();

        assertEquals(expectZoneId, staffRepository.findById(staff.getId()).orElseThrow(NoSuchFieldError::new).getZoneId());
    }
}
