package com.twuc.webApp.web.request;

import javax.validation.constraints.NotBlank;

public class UpdateStaffZoneIdRequest {
    @NotBlank
    private String zoneId;

    public String getZoneId() {
        return zoneId;
    }
}
