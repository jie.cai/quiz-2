package com.twuc.webApp.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.zone.ZoneRulesProvider;
import java.util.Set;

@RestController
@RequestMapping("/api/timezones")
public class TimezonesController {
    @GetMapping
    public Set<String> getTimezones() {
        return ZoneRulesProvider.getAvailableZoneIds();
    }
}
