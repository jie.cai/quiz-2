package com.twuc.webApp.web;

import com.twuc.webApp.dao.StaffRepository;
import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.web.request.UpdateStaffZoneIdRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/staffs")
public class StaffController {

    private final StaffRepository staffRepository;

    public StaffController(StaffRepository staffRepository) {
        this.staffRepository = staffRepository;
    }

    @PostMapping
    public ResponseEntity createStaff(
            @RequestBody @Valid Staff staff
    ) {
        Staff savedStaff = staffRepository.save(staff);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header("Location", "/api/staffs/" + savedStaff.getId())
                .build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Staff> getStaff(
            @PathVariable Long id
    ) {
        Optional<Staff> staff = staffRepository.findById(id);
        if (staff.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(staff.get());
        } else {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
    }

    @GetMapping
    public List<Staff> getStaffs() {
        return staffRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/timezone")
    public ResponseEntity<Object> updateZoneId(
            @PathVariable Long id,
            @RequestBody @Valid UpdateStaffZoneIdRequest request
    ) {
        Optional<Staff> staff = staffRepository.findById(id);
        if (staff.isPresent()) {
            staff.get().setZoneId(request.getZoneId());
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .build();
        } else {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
    }
}
